package com.threadjava.postReactions;

import com.threadjava.post.dto.PostDetailsDto;
import com.threadjava.postReactions.dto.ReceivedPostReactionDto;
import com.threadjava.postReactions.dto.ResponsePostReactionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/postreaction")
public class PostReactionController {
    @Autowired
    private PostReactionService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @PutMapping
    public Optional<ResponsePostReactionDto> setReaction(@RequestBody ReceivedPostReactionDto postReaction){
        postReaction.setUserId(getUserId());
        var reaction = postsService.setReaction(postReaction);

        if (reaction.isPresent() && reaction.get().getUserId() != getUserId()) {
            // notify a user if someone (not himself) liked his post
            template.convertAndSend("/topic/like", "Your post was liked!");
        }
        return reaction;
    }

    @GetMapping("/{id}")
    public Optional<ResponsePostReactionDto> getReaction(@PathVariable UUID id) {
        Logger log = LoggerFactory.getLogger(PostReactionController.class);
        Optional<ResponsePostReactionDto> postReactionDto = postsService.getReaction(getUserId(), id);
        log.info(String.valueOf(postReactionDto));
        return postReactionDto;
    }
}
