package com.threadjava.post;


import com.threadjava.post.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.threadjava.auth.TokenService.getUserId;

@RestController
@RequestMapping("/api/posts")
public class PostsController {
    @Autowired
    private PostsService postsService;
    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public List<PostListDto> get(@RequestParam(defaultValue="0") Integer from,
                                 @RequestParam(defaultValue="10") Integer count,
                                 @RequestParam(required = false) UUID userId,
                                 @RequestParam Boolean showOwn,
                                 @RequestParam Boolean showLikedByUser,
                                 @RequestParam UUID senderId) {

        return postsService.getAllPosts(from, count, userId, showOwn, showLikedByUser, senderId);
    }

    @GetMapping("/{id}")
    public PostDetailsDto get(@PathVariable UUID id) {
        return postsService.getPostById(id);
    }

    @PostMapping
    public PostCreationResponseDto post(@RequestBody PostCreationDto postDto) {
        postDto.setUserId(getUserId());
        var item = postsService.create(postDto);
        template.convertAndSend("/topic/new_post", item);
        return item;
    }

    @PutMapping
    public void put(@RequestBody ReceivedPostUpdateDto receivedPostUpdateDto) {
        postsService.update(receivedPostUpdateDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable UUID id) {
        postsService.delete(id);
    }
}
