package com.threadjava.post.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class ReceivedPostUpdateDto {
    private UUID id;
    private String body;

    public UUID getId() {
        return id;
    }

    public String getBody() {
        return body;
    }
}
