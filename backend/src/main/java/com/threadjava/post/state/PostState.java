package com.threadjava.post.state;

public enum PostState {
    ACTIVE, DELETED
}
