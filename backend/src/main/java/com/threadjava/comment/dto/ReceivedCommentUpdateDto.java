package com.threadjava.comment.dto;

import lombok.Data;
import java.util.UUID;

@Data
public class ReceivedCommentUpdateDto {
    private UUID id;
    private String body;

    public UUID getId() {
        return id;
    }

    public String getBody() {
        return body;
    }
}
