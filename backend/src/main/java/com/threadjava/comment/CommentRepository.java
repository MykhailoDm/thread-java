package com.threadjava.comment;

import com.threadjava.comment.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {
    List<Comment> findAllByPostId(UUID postId);

    @Transactional
    @Modifying
    @Query("update Comment c set c.body = :body, c.updatedAt = :updatedAt where c.id = :id")
    void updateCommentSetBodyForId(@Param("body") String body, @Param("updatedAt") Date updatedAt, @Param("id") UUID id);
}