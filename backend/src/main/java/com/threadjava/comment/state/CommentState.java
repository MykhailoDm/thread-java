package com.threadjava.comment.state;

public enum CommentState {
    ACTIVE, DELETED
}
