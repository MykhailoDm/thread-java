import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const updatePost = (oldPost, newBody) => async (dispatch, getRootState) => {
  const updatedPost = post => ({
    ...post,
    body: newBody
  });

  postService.updatePost(oldPost.id, newBody);

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => (post.id !== oldPost.id ? post : updatedPost(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === oldPost.id) {
    dispatch(setExpandedPost(updatedPost(expandedPost)));
  }
};

const updateComment = (oldComment, newBody) => async (dispatch, getRootState) => {
  const updatedComment = deprecatedComment => ({
    ...deprecatedComment,
    body: newBody
  });

  const commentPost = post => ({
    ...post,
    comments: post.comments.map(comment => (comment.id !== oldComment.id ? comment : updatedComment(comment)))
  });

  commentService.updateComment(oldComment.id, newBody);
  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const updated = posts.map(post => (post.id !== expandedPost.id ? post : commentPost(expandedPost)));

  dispatch(setPosts(updated));

  dispatch(setExpandedPost(commentPost(expandedPost)));
};

const deletePost = postId => async (dispatch, getRootState) => {
  postService.deletePost(postId);

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const newPosts = posts.filter(post => post.id !== postId);

  dispatch(setPosts(newPosts));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(undefined));
  }
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  commentService.deleteComment(commentId);

  const {
    posts: { posts, expandedPost }
  } = getRootState();

  const newCommentCount = expandedPost.commentCount - 1;

  const newComments = expandedPost.comments
    .filter(comment => comment.id !== commentId);

  const newExpandedPost = {
    ...expandedPost,
    comments: newComments,
    commentCount: newCommentCount
  };

  dispatch(setExpandedPost(newExpandedPost));

  const updatedPosts = posts.map(post => (post.id !== expandedPost.id ? post : newExpandedPost));

  dispatch(setPosts(updatedPosts));
};

const likePost = postId => async (dispatch, getRootState) => {
  const postReaction = await postService.getReaction(postId);
  const response = await postService.likePost(postId);
  const diff = response?.id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
  const dislikeDiff = postReaction !== null && !postReaction.isLike ? -1 : 0;

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const postReaction = await postService.getReaction(postId);
  const response = await postService.dislikePost(postId);
  const diff = response?.id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
  const likeDiff = postReaction !== null && postReaction.isLike ? -1 : 0;

  const mapLikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
    likeCount: Number(post.likeCount) + likeDiff
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  updatePost,
  updateComment,
  deletePost,
  deleteComment,
  likePost,
  dislikePost,
  addComment
};
