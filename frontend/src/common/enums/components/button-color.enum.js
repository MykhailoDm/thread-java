const ButtonColor = {
  TEAL: 'teal',
  BLUE: 'blue',
  PURPLE: 'purple',
  GRAY: 'grey'
};

export { ButtonColor };
