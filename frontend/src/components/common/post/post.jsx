import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label } from 'src/components/common/common';
import { useDispatch } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';

import styles from './styles.module.scss';

import Delete from '../delete-consent/delete';

const Post = ({ post, currentUserId, onPostLike, onPostDislike, onExpandedPostToggle, onUpdatePost, sharePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const [showDelete, setShowDelete] = React.useState(undefined);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handlePostUpdate = () => onUpdatePost(post);

  const dispatch = useDispatch();
  const handlePostDelete = React.useCallback(postId => (
    dispatch(threadActionCreator.deletePost(postId))
  ), [dispatch]);

  const onPostDelete = () => handlePostDelete(id);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          {currentUserId === user.id && (
            <Label
              basic
              size="medium"
              as="a"
              className={styles.closeBtn}
              onClick={() => setShowDelete(post)}
            >
              <Icon name={IconName.CLOSE} />
            </Label>
          )}
          {showDelete && (
            <Delete
              contentName="Post"
              close={() => setShowDelete(undefined)}
              handleDelete={() => onPostDelete()}
            />
          )}
        </Card.Meta>
        <Card.Description>{body}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
        {currentUserId === user.id && (
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={handlePostUpdate}
          >
            <Icon name={IconName.PENCIL} />
          </Label>
        )}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  currentUserId: PropTypes.string.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  onUpdatePost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
