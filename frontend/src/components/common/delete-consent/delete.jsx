import * as React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'src/components/common/common';
import {
  ButtonType,
  ButtonColor
} from 'src/common/enums/enums';

const Delete = ({ contentName, close, handleDelete }) => {
  const onDelete = () => {
    handleDelete();
    close();
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header>
        <span>{`Do you want to delete this ${contentName}?`}</span>
      </Modal.Header>
      <Modal.Actions>
        <Button type={ButtonType.BUTTON} basic color={ButtonColor.BLUE} inverted onClick={onDelete}>
          Yes
        </Button>
        <Button type={ButtonType.BUTTON} color={ButtonColor.GRAY} inverted onClick={() => close()}>
          No
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

Delete.propTypes = {
  contentName: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired
};

export default Delete;
