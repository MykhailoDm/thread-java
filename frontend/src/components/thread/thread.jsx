import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { threadActionCreator } from 'src/store/actions';
import { image as imageService } from 'src/services/services';
import { Post, Spinner, Checkbox } from 'src/components/common/common';
import { ExpandedPost, SharedPostLink, AddPost } from './components/components';

import styles from './styles.module.scss';
import UpdatedPost from './components/updated-post/updated-post';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  showOwn: true,
  showLikedByUser: false,
  senderId: undefined
};

const Thread = () => {
  const { posts, hasMorePosts, expandedPost, userId } = useSelector(state => ({
    posts: state.posts.posts,
    hasMorePosts: state.posts.hasMorePosts,
    expandedPost: state.posts.expandedPost,
    userId: state.profile.user.id
  }));
  const [sharedPostId, setSharedPostId] = React.useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = React.useState(false);
  const [showOtherPosts, setShowOtherPosts] = React.useState(false);
  const [showLikedByMe, setShowLikedByMe] = React.useState(false);
  const [updatedPost, setUpdatedPost] = React.useState(undefined);
  const dispatch = useDispatch();

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handlePostAdd = React.useCallback(postPayload => (
    dispatch(threadActionCreator.createPost(postPayload))
  ), [dispatch]);

  const handlePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadPosts(filtersPayload));
  };

  const handleMorePostsLoad = filtersPayload => {
    dispatch(threadActionCreator.loadMorePosts(filtersPayload));
  };

  const toggleShowOwnPosts = () => {
    if (!showOwnPosts) { setShowOtherPosts(false); }
    setShowOwnPosts(!showOwnPosts);
    postsFilter.senderId = userId;
    postsFilter.showOwn = true;
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOtherPosts = () => {
    if (!showOtherPosts) { setShowOwnPosts(false); }
    setShowOtherPosts(!showOtherPosts);
    postsFilter.senderId = userId;
    postsFilter.showOwn = false;
    postsFilter.userId = showOtherPosts ? undefined : userId;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedByMe = () => {
    postsFilter.showLikedByUser = !showLikedByMe;
    setShowLikedByMe(!showLikedByMe);
    postsFilter.senderId = userId;
    postsFilter.userId = showOtherPosts || showOwnPosts ? userId : undefined;
    postsFilter.showOwn = showOwnPosts;
    postsFilter.from = 0;
    handlePostsLoad(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const getMorePosts = () => {
    postsFilter.senderId = userId;
    handleMorePostsLoad(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => setSharedPostId(id);

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost onPostAdd={handlePostAdd} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Do not show my posts"
          checked={showOtherPosts}
          onChange={toggleShowOtherPosts}
          style={{ marginLeft: '0.5em' }}
        />
        <Checkbox
          toggle
          label="Show liked by me"
          checked={showLikedByMe}
          onChange={toggleShowLikedByMe}
          style={{ marginTop: '0.7em' }}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Spinner key="0" />}
      >
        {posts.map(post => (
          <Post
            post={post}
            currentUserId={userId}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onExpandedPostToggle={handleExpandedPostToggle}
            onUpdatePost={setUpdatedPost}
            sharePost={sharePost}
            key={post.id}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          currentUserId={userId}
          onUpdatePost={setUpdatedPost}
        />
      )}
      {updatedPost && (
        <UpdatedPost
          post={updatedPost}
          close={() => setUpdatedPost(undefined)}
        />
      )}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

export default Thread;
