import * as React from 'react';
import PropTypes from 'prop-types';
import { Modal, Input } from 'src/components/common/common';
import { useDispatch } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';

import styles from '../update-styles/styles.module.scss';

const UpdatedComment = ({ comment, close }) => {
  const dispatch = useDispatch();
  const [input, setInput] = React.useState(comment.body);

  const handleCommentUpdate = React.useCallback((oldComment, newBody) => (
    dispatch(threadActionCreator.updateComment(oldComment, newBody))
  ), [dispatch]);

  const onPostUpdate = () => {
    handleCommentUpdate(comment, input);
    close();
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Update comment</span>
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'pencil',
            content: 'Update',
            onClick: onPostUpdate
          }}
          value={input}
          onChange={event => setInput(event.target.value)}
        />
      </Modal.Content>
    </Modal>
  );
};

UpdatedComment.propTypes = {
  comment: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdatedComment;
