import * as React from 'react';
import PropTypes from 'prop-types';
import { Modal, Input } from 'src/components/common/common';
import { threadActionCreator } from 'src/store/actions';
import { useDispatch } from 'react-redux';

import styles from '../update-styles/styles.module.scss';

const UpdatedPost = ({ post, close }) => {
  const dispatch = useDispatch();
  const [input, setInput] = React.useState(post.body);

  const handlePostUpdate = React.useCallback((oldPost, newBody) => (
    dispatch(threadActionCreator.updatePost(oldPost, newBody))
  ), [dispatch]);

  const onPostUpdate = () => {
    handlePostUpdate(post, input);
    close();
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Update post</span>
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'pencil',
            content: 'Update',
            onClick: onPostUpdate
          }}
          value={input}
          onChange={event => setInput(event.target.value)}
        />
      </Modal.Content>
    </Modal>
  );
};

UpdatedPost.propTypes = {
  post: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdatedPost;
