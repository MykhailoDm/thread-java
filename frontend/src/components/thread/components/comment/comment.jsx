import * as React from 'react';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon, Label } from 'src/components/common/common';
import { commentType } from 'src/common/prop-types/prop-types';
import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import styles from './styles.module.scss';
import { IconName } from '../../../../common/enums/components/icon-name.enum';
import Delete from '../../../common/delete-consent/delete';

const Comment = ({ comment: { id, body, createdAt, user }, onCommentUpdate, currentUserId }) => {
  const [showDelete, setShowDelete] = React.useState(false);

  const dispatch = useDispatch();
  const handleCommentDelete = React.useCallback(commentId => (
    dispatch(threadActionCreator.deleteComment(commentId))
  ), [dispatch]);

  const onCommentDelete = () => {
    handleCommentDelete(id);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{getFromNowTime(createdAt)}</CommentUI.Metadata>
        {currentUserId === user.id && (
          <Label
            basic
            size="medium"
            as="a"
            className={styles.closeBtn}
            onClick={() => setShowDelete(true)}
          >
            <Icon name={IconName.CLOSE} />
          </Label>
        )}
        <CommentUI.Text>{body}</CommentUI.Text>
        {currentUserId === user.id
        && (
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => onCommentUpdate()}
          >
            <Icon name={IconName.PENCIL} />
          </Label>
        )}
      </CommentUI.Content>
      {showDelete && (
        <Delete
          handleDelete={() => onCommentDelete()}
          contentName="Comment"
          close={() => setShowDelete(false)}
        />
      )}
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentUpdate: PropTypes.func.isRequired,
  currentUserId: PropTypes.string.isRequired
};

export default Comment;
